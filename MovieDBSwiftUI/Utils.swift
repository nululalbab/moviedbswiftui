//
//  Utils.swift
//  MovieDBSwiftUI
//
//  Created by Najibullah Ulul Albab on 20/06/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import Foundation


class Utils {
    
    static let jsonDecoder: JSONDecoder = {
        let jsonDecoder = JSONDecoder()
        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
        jsonDecoder.dateDecodingStrategy = . formatted(dateFormatter)
        
        return jsonDecoder
    }()
    
    static let dateFormatter: DateFormatter = {
       let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-mm-dd"
        return dateFormatter
    }()
}
