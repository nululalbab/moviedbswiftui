//
//  Movie.swift
//  MovieDBSwiftUI
//
//  Created by Najibullah Ulul Albab on 20/06/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import Foundation

struct MovieResponse: Decodable {
    let results: [Movie]
}

struct Movie: Decodable{
    let id: Int
    let title: String
    let backdropPath: String?
    let posterPath: String?
    let overview: String
    let voteOverage: Double
    let voteCount: Int
    let runtime: Int?
    
    var backdropURL: URL{
        return URL(string: "https://image.tmdb.org/t/p/w500\(backdropPath ?? "")")!
    }
}
